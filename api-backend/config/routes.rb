Rails.application.routes.draw do

  namespace :v1 do
    resources :users, only: [:create] do
      resources :repositories, only: [:index, :update]
      get '/repositories/:search', to: 'repositories#search'
    end
  end
end
