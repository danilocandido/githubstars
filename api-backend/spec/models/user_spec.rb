require 'rails_helper'

RSpec.describe User, type: :model do

  context 'associations' do
    it { is_expected.to have_many(:repositories).dependent(:destroy) }
  end

  context 'validations' do
    subject { create(:user, username: 'luciana') }
    it { is_expected.to validate_presence_of(:username) }
    it { is_expected.to validate_uniqueness_of(:username) }
  end

  context 'table fields' do
    it { is_expected.to have_db_column(:username).of_type(:string) }
    it { is_expected.to have_db_index(:username) }
  end

  context 'factories' do
    it { expect(build(:user)).to be_valid }
  end
end
