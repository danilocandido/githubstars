require 'rails_helper'

RSpec.describe V1::RepositoriesController, type: :controller do

  let(:user) { create(:user) }

  describe 'GET #index' do
    context 'with valid attributes' do
      it 'valid response' do
        get :index, { :params => { user_id: user.id }, :format => :json }
  
        expect(response.content_type).to eq "application/json"
        expect(response).to have_http_status(:ok)
      end

      it 'return an array of repositories' do
        5.times do
          create(:repository, user: user)
        end
        get :index, { :params => { user_id: user.id }, :format => :json }

        expect(user.repositories.size).to eq 5
      end
    end

    context 'with invalid attributes' do
      it 'does not return an array of repositories' do
        get :index, { :params => { user_id: -1 }, :format => :json }
        expect(user.repositories).to be_empty
      end
    end
  end

  describe 'GET #search' do
    context 'with valid attributes' do
      it 'valid response' do
        get :search, { :params => { user_id: user.id, search: 'repo-name' }, :format => :json }
  
        expect(response.content_type).to eq "application/json"
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'PUT #update' do
    before :each do
      @repository = create(:repository, user: user, tags: "libs, languages")
    end

    context 'valid attributes' do
      it 'valid response' do
        put :update, :params => { user_id: user.id, id: @repository.id, :tags => "back, front, full" }
  
        expect(response.content_type).to eq "application/json"
        expect(response).to have_http_status(:ok)
      end
    end

    it 'change tags attribute' do
      put :update, :params => { user_id: user.id, id: @repository.id, :tags => "back, front, full" }
      @repository.reload
      expect(@repository.tags).not_to eq 'libs, languages'
      expect(@repository.tags).to eq 'back, front, full'
    end
  end
end
