require 'rails_helper'

RSpec.describe V1::UsersController, type: :controller do

  describe 'POST #create' do
    context 'with valid attributes' do
      it 'creates a new user with repositories' do
        expect{
          post :create, { :params => { :username => "Lucy Name" }, :format => :json }
        }.to change(User, :count).by(1)

        expect(response.content_type).to eq "application/json"
        expect(response).to have_http_status(:created)
      end
    end

    context 'with invalid attributes' do
      it 'does not create a new user' do
        expect{
          post :create, { :params => { :username => "" }, :format => :json }
        }.to_not change(User, :count)
      end
    end
  end
end
