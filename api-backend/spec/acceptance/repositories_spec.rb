require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Repositories" do
  header 'Accept', 'application/json'
  header 'Content-Type', 'application/json'

  route "/v1/users/:user_id/repositories", "Retrieve Repositories" do
    attribute :user_id, "ID do usuário", require: true

    get "last 15 repositórios by user" do
      let(:user) { User.create(username: 'danilo') }
      
      example 'Get a list of repositories' do
        user.repositories.create(name: 'name', user: user)

        do_request(user_id: user.id)

        expect(status).to eq 200
        expect(response_body).to eq(user.repositories.to_json)
      end
    end
  end

  route "/v1/users/:user_id/repositories/:search", "Retrieve search repositories by name" do
    attribute :user_id, "ID do usuário", require: true
    attribute :search, "Descrição do atributo TAG do repositório", require: true

    get "repositórios by filter" do
      let(:user) { User.create(username: 'danilo') }
      
      example 'Get a list repositories' do
        user.repositories.create(name: 'repositorio-ruby', user: user)

        do_request(user_id: user.id, search: 'repositorio-ruby')

        expect(status).to eq 200
      end
    end
  end

  route '/v1/users/:user_id/repositories/:id', 'Update of Repository' do
    attribute :user_id, "User ID"
    attribute :id, "Repository ID", :required => true
    attribute :tags, 'description of a tag'

    put 'update tag' do
      let(:user) { User.create(username: 'danilo') }
      
      example 'updating a repository' do
        user.repositories.create(name: 'repositorio-ruby', user: user)

        do_request(user_id: user.id, id: user.repositories.last.id)

        expect(status).to eq(200)
      end
    end
  end
end