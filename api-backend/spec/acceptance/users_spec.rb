require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Users" do
  header 'Accept', 'application/json'
  header 'Content-Type', 'application/json'

  route "/v1/users/", "Create a User" do
    attribute :username, "Nome do usuário", require: true

    let(:request) { {username: 'danilo-candido'} }
    
    post "last 15 repositórios by user" do
      let(:raw_post) { params.to_json }
      example 'creating user' do

        do_request(request)
  
        expect(status).to eq 201
      end
    end
  end
end
