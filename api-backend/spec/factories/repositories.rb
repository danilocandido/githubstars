require 'faker'

FactoryBot.define do
  factory :repository do
    name { Faker::Name.name }
    description "Repositorio do factory bot"
    language "ruby elixir"
    url "github.com.br/danilo"
    user
  end
end
