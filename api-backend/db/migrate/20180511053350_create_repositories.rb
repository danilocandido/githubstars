class CreateRepositories < ActiveRecord::Migration[5.2]
  def change
    create_table :repositories do |t|
      t.string :name, null: false
      t.string :description
      t.string :url
      t.string :tags, default: ''
      t.jsonb :language, null: false, default: {}

      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
