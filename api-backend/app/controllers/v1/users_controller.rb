class V1::UsersController < ApplicationController

  def create
    user = User.create_with_repositories(params[:username])
    render json: user, status: :created
  end
end
