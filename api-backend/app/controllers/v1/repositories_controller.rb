class V1::RepositoriesController < ApplicationController

  def index
    repositories = Repository.where(user_id: params[:user_id]).last(15)
    render json: repositories, status: :ok
  end
  
  def search
    render json: Repository.search(repository_params), status: :ok
  end

  def update
    repository = Repository.find(params[:id])
    repository.update(tags: params[:tags])
    render json: repository, status: :ok
  end

  private
    def repository_params
      params.permit(:user_id, :search)
    end
end
