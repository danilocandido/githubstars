class User < ApplicationRecord
  include Github
  include UserQuery

  has_many :repositories, dependent: :destroy
  validates :username, uniqueness: true, presence: true, allow_blank: false

  def self.create_with_repositories(username)
    user = User.find_or_create_by(username: username)
    if user.repositories.empty?
      return user unless started_repos = started_repositories({ username: username })
      started_repos.each do |repo|
        user.repositories.create repositories_params(repo)
      end
    end
    user
  end

  def self.started_repositories(username)
    response = Github::Client.query(UserProfileQuery, variables: username)
    result = response.data.user.to_h
    return if result.empty?
    result.fetch('starredRepositories')['nodes']
  end

  def self.repositories_params(repository)
    { url: repository['url'], 
      name: repository['name'], 
      description: repository['description'], 
      language: repository['languages'].to_json 
    }
  end
end
