module UserQuery
  extend ActiveSupport::Concern

  UserProfileQuery = Github::Client.parse <<-'GRAPHQL'
    query($username: String!) {
      user(login: $username) {
        starredRepositories(first: 100) {
          totalCount
          nodes {
            name
            description
            url
            languages(first: 3) {
              nodes{
                name
              }
            }
          }
        }
      }
    }
  GRAPHQL
end
