class Repository < ApplicationRecord
  belongs_to :user

  validates :name, uniqueness: true, presence: true

  def self.search(params)
    where(user_id: params[:user_id])
    .where("tags like ?", "%#{params[:search]}%")
  end
end
