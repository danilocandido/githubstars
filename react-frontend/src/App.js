import React, { Component } from 'react';
import Header from './components/Header';
import './App.css';
import SearchPage from './components/SearchPage';

class App extends Component {
  render() {
    return (
      <div id="root">
        <Header/>
        <div className="App">
          <SearchPage/>
        </div>
      </div>
    );
  }
}

export default App;
