import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import RepositoryList from './components/RepositoryList'
import {Router, Route, browserHistory} from 'react-router';

ReactDOM.render(
  (
    <Router history={browserHistory}>
      <Route path="/" component={App}/>
      <Route path="/repositories" component={RepositoryList} />
    </Router>
  ), 
  document.getElementById('root')
);
