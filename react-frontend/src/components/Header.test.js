import React from 'react';
import Header from './Header';
import { shallow } from 'enzyme';

describe('<Header />', () => {
  it('renders', () => {
    const wrapper = shallow(<Header name="header-aplication" />);
    expect(wrapper.find('h1').text()).toEqual('GithubStars');
  });
});
