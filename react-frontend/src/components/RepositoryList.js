import React, { Component } from 'react';
import TableRepository from './TableRepository'
import Header from './Header';
import '../index.css';
import {API_URI} from '../config';

export default class RepositoryList extends Component {

  constructor(props) {
    super(props);
    this.state = { userID: this.props.location.state.userID, repos: [], tagSearch: ''};
  }

  componentWillMount() {
    fetch(`${API_URI}/users/${this.state.userID}/repositories`)
      .then(response => response.json())
      .then(repos => this.setState({repos: repos}));
  }

  filterRepos(event) {
    this.setState({tagSearch: event.target.value});
    fetch(`${API_URI}/users/${this.state.userID}/repositories/${event.target.value.substr(0, 20)}`)
      .then(response => response.json())
      .then(repos => this.setState({repos: repos}));
  }

  render() {
    return(
      <div>
        <Header />

        <div className="container">
          <input type="text" value={this.state.tagSearch} 
                 ref={(input) => this.tagSearch = input}
                 placeholder="search by tag"
                 onChange={this.filterRepos.bind(this)} className="form-control"/>

          <br/>
          
          <TableRepository repositories={this.state.repos} userID={this.state.userID}/>        
        </div>
      </div>
    );
  }
}
