import React from 'react';
import ReactLoading from 'react-loading';
import { Section, Title, Article } from "./generic";

const Loading = ({ type }) => (
  <Section>
    <Article>
      <ReactLoading type={type} color="#ff" height={200} width={200}/>
    </Article>
    <Title>Getting the repositories list from github</Title>
  </Section>
)

export default Loading;
