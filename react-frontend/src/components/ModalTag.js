import React, { Component } from 'react';
import ReactModal from 'react-modal';
import PropTypes from 'prop-types';
import { API_URI } from '../config';

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
};

export default class ModalTag extends Component {

  constructor() {
    super();
    this.state = { showModal: false, repo: {}, tags: '', message: '' };
  }

  componentWillMount() {
    ReactModal.setAppElement('body');
  }

  onChangeTags(event) { 
    this.setState({ tags: event.target.value }) 
    this.props.repo.tags = event.target.value;
    this.setState({message: ''});
  }

  removeDuplicatedTagsAsString(){
    let arrayTags = this.tag.value.split(',').map(val => val.trim());
    if(arrayTags[arrayTags.length - 1] === ""){
      arrayTags.pop();
    }
    let setTags = new Set(arrayTags);
    return Array.from(setTags).toString();
  }

  editRepo(event) {
    event.preventDefault();
    let tags = this.tag.value;

    if(!tags.trim()){
      this.setState({ message: 'Type a valid input' });
      return;
    }
    
    let tagsJSON = this.removeDuplicatedTagsAsString()

    const requestInfo = {
      method: 'PUT',
      body: JSON.stringify({tags: tagsJSON}),
      headers: new Headers({
        'Content-type': 'application/json'
      })
    }
    
    fetch(`${API_URI}/users/${this.props.userID}/repositories/${this.props.repo.id}`, requestInfo)
      .then(response => {
        if(response.ok){
         return response.json();
        }
      })
      .then(repo => {
        this.setState({repo: repo});
        let index = this.props.repositories.findIndex(x => x.id === repo.id);
        this.props.repositories[index] = repo;
        this.props.closeModal();
      })
      .catch(error => this.setState({msg: error.message}));
  }

  render() {
    return(
      <ReactModal isOpen={this.props.showModal} contentLabel="Modal Tags" style={customStyles} >
        <span>{this.state.message}</span>
        <form onSubmit={this.editRepo.bind(this)} className="form-horizontal">
          <div className="form-group">
            <label>edit tags for {this.props.repo.name}</label>
            <input type="text" value={this.props.repo.tags} 
                               ref={ (input) => this.tag = input }
                               onChange={this.onChangeTags.bind(this)} 
                               id="disabledTextInput" className="form-control"/>
          </div>

          <button type="submit" className="btn btn-default">Save</button>
          <button onClick={this.props.closeModal} className="btn btn-default">Cancel</button>
        </form>
      </ReactModal>
    );
  }
}

//Validations
ModalTag.propTypes = {
  repo: PropTypes.object,
  userID: PropTypes.number,
  showModal: PropTypes.bool,
  repositories: PropTypes.array,
  closeModal: PropTypes.func
};
