import React from 'react';
import ReactDOM from 'react-dom';
import Loading from './Loading';
import { Section, Title, Article } from "./generic";

it('<Loading>', () => {
  const component = document.createElement('Section');
  ReactDOM.render(<Section />, component);
  ReactDOM.unmountComponentAtNode(component);
});
