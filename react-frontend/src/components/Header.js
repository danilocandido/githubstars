import React from 'react';

const Header = () => (
  <header className="header-aplication">
    <h1 >
      GithubStars
    </h1>
  </header>
);

export default Header;
