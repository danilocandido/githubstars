import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import Loading from './Loading';
import { API_URI } from '../config';

export default class SearchPage extends Component {

  constructor() {
    super()
    this.state = { message: '', isLoading: false }
  }

  getRepositories(event) {
    event.preventDefault();
    let username = event.target.username.value;

    if(!username.trim()){
      this.setState({ message: 'Type a valid input' });
      return;
    }

    const requestInfo = {
      method: 'POST',
      body: JSON.stringify({ username: username }),
      headers: new Headers({
        'Content-type': 'application/json'
      })
    }

    this.setState({ isLoading: true });

    fetch(`${API_URI}/users`, requestInfo)
      .then(response => {
        if(response.ok) {
         return response.json(); 
        }
      })
      .then(user => {
        browserHistory.push({ pathname: '/repositories', state: { userID: user.id } });
      })
      .catch(error => this.setState({ message: error.message }));
  }

  render() {
    if(this.state.isLoading) return(<Loading type={'cubes'}/>);

    return(
      <div>
        <span>{this.state.message}</span>
        <form onSubmit={this.getRepositories.bind(this)} className="form-horizontal">

          <div className="input-group">
            <span className="input-group-addon" id="basic-addon3">https://github.com/</span>
            <input type="text" className="form-control input-lg" id="username" aria-describedby="basic-addon3"/>
          </div>
          <br/>

          <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
              <button type="submit" className="btn btn-default btn-lg">get repositories</button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
