import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ModalTag from './ModalTag';

export default class TableRepository extends Component {

  constructor() {
    super();
    this.state = { showModal: false, repo: {} };

    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }

  handleOpenModal () { 
    this.setState({ showModal: true }) 
  }
  
  handleCloseModal () { 
    this.setState({ showModal: false }) 
  }

  currentRepository(repo) { 
    this.setState({ repo, showModal: true }) 
  }

  formatLanguages(language) {
    return JSON.parse(language).nodes.map(lang => `${lang.name} `) 
  }

  formatTags(tags) {
    return tags ? tags.split(',').map(tag => `#${tag} `) : '';
  }

  render() {
    return(
      <div>
        <table className="table table-striped table-bordered table-hover">
          <tbody>
            <tr>
              <th>Repository</th>
              <th>Description</th>
              <th>Language</th>
              <th>tags</th>
              <th></th>
            </tr>
            {
              this.props.repositories.map(repo =>
              <tr key={repo.id}>
                <td>{repo.name}</td>
                <td> {repo.description} </td>
                <td> {this.formatLanguages(repo.language)} </td>
                <td> {this.formatTags(repo.tags)} </td>
                <td><button type="button" onClick={this.currentRepository.bind(this, repo)} className="btn btn-link">edit</button></td>
              </tr>
              )
            }
          </tbody>
        </table>

        <ModalTag repo={this.state.repo} 
                  showModal={this.state.showModal}
                  userID={this.props.userID}
                  repositories={this.props.repositories}
                  closeModal={this.handleCloseModal}/>
      </div>
    );
  }
}

//Validations
TableRepository.propTypes = {
  repositories: PropTypes.array
};
