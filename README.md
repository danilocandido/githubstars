# GithubStars

Adiciona um label ou tag para marcar o repositório de modo que ele possa ser filtrado. 
Um repositório chamado react e gostaria de adicionar as tags javascript e frontend.
Como o Github não possui essa funcionalidade, esse projeto pode ajudá-lo.

## Projeto

 api-backend (raiz do projeto)  
 |--- api-backend ( api rails 5 )  
 |--- react-frontend (app reactjs)  
 |--- README.MD

### Tecnologias

* **Ruby (2.5)**
* **Javascript(es6)**
* **React**  

Outras Tecnologias
* Rails (5.2)
* graphql (ruby)
* Postgres

Tecnologias para Testes
* Rspec
* Factory Bot
* Shoulda Matchers
* Rspec api documentation (API Blueprint)

### Pré-requisitos

O que é preciso para rodar o projeto

```
NodeJS, Ruby(2.5), Postgres 9+
```

## Rodando os projetos

#### BackEnd > api-backend

Abra e edite o arquivo **.env** dentro da pasta /githubstars/react-frontend.

githubstars/react-frontend/.env
> GITHUB_ACCESS_TOKEN=coloque_aqui_seu_token_do_github

**Importante: a aplicação só irá rodar se o tocken estiver inserido.**

```
bundle install
bundle exec rails server
```
> Booting Puma  
> => Rails 5.2.0 application starting in development  
> => Run `rails server -h` for more startup options  
> Puma starting in single mode...  
> * Version 3.11.4 (ruby 2.5.0-p0), codename: Love Song  
> * Min threads: 5, max threads: 5  
> * Environment: development  
> * Listening on tcp://0.0.0.0:4000  

#### FrontEnd > react-frontend
```
npm install
npm start

```
> Compiled successfully!  
>
> You can now view react-frontend in the browser.  
>  Local:            http://localhost:3000/  
>  On Your Network:  http://192.168.88.88:3000/  


## Rodando os testes

Teste: 'models, controllers, acceptance, factories' dentro da pasta spec
```
bundle exec rspec
```

Testes frotEnd
```
npm test
```

Documentação **API Blueprint**  
diretório salvo > **api-backend/doc/api/index.apib**
```
bundle exec rails docs:generate
```

## Explicação

Aqui será descrito como foi implementada cada funcionalidade.

## Criado com

* [Rails API](http://guides.rubyonrails.org/v5.0/api_app.html) - Rails API-only
* [ReactJS](https://github.com/facebook/create-react-app) -Create React App

## Rails API - BackEND

* Usei o rails 5.2 juntamente com ruby 2.5 para criar a api do backend.  
* Usei Postgres porque com ele é possível criar uma coluna jsonb para salvar json e o Rails 5.2 integra essa funcionalide com o postgres.  
* Usei graphql-client no backend para resgatar os repositórios do usuário. (classe Github.rb)
* Test Models -> api-backend/spec/models
* Test Controllers -> api-backend/spec/controllers
* API Blueprint - api-backend/spec/acceptance
* API RAILS
  * os controllers na pasta controllers/v1
  * assim como os testes apontam para a rota correta
  * CORS - habilitado somente para :get, :post, :put
  * A documentação blueprint é gerada a partir dos testes na pasta /spec/acceptance
 ```
   #Foi criado as rotas com os resources
    namespace :v1 do
    resources :users, only: [:create] do
      resources :repositories, only: [:index, :update]
      get '/repositories/:search', to: 'repositories#search'
    end
  ```
* O arquivo .env é usado para colocar seu token do github.


## ReactJS - FrontEnd

* Usei o create-react-app para criar o projeto
* Adicionei as dependências de teste enzyme, enzyme-adapter-react-16
* Criei o diretório components para inserir os componentes que foram criados
* react-router foi usado para criar as rotas do frontend
* Foi criado o componente Search que é a primeira tela de pesquisa
* Foi criado o component Loading the é exibido na transição das telas
* Foi criado o componente RepositoryList que carrega os repositórios depois de inserido e permite pesquisar as tags
* Foi criado o component TableRepository que mostra os dados retornados e consulta
* Foi criado o component ModalTag que é o modal de edição das tags
* Foi criado o component Header com contém o header a aplicação


## Autor

* **Danilo Cândido** - *05/2018* - danilod07@gmail.com

## Plus Docker-Compose

Os containers são buildados com sucesso a aplicação.
as portas estão espostas para o uso entre eles.
Porém a frontend não se comunica com o backend. (Não entendi o porque) :/


expus as aplicações para acessar localmente.
frontend: http://localhost:3001
backend: http://localhost:4001

* Cada aplicação(back e front) possui seu próprio *Dockerfile*
* Criei o *docker-compose* que builda dois containers automáticamente e cria a imagem do postgress e a comunicação entre eles é feita pela network -> gitstars-network

em config/database.yml colocar os parametros abaixo.

```
username: postgres
password: 
host: db
```

dentro da raiz do projeto rodar o comando abaixo
> docker-compose up --build



```
CONTAINERS RODANDO
>docker-compose ps
danilo@dan: docker-compose ps
           Name                         Command              State           Ports         
-------------------------------------------------------------------------------------------
dbchallenge_db_1         docker-entrypoint.sh postgres   Up      0.0.0.0:5433->5432/tcp
reactchallenge_frontend_1   npm run start                   Up      0.0.0.0:3001->3000/tcp
apichallenge_webapi_1     puma -C config/puma.rb          Up      0.0.0.0:4001->4000/tcp

```
